import type { AppProps } from "next/app";
import Image from "next/image";
import {
  ChakraProvider,
  extendTheme,
  Box,
  Center,
  Text,
  Flex,
} from "@chakra-ui/react";
import { ApolloProvider } from "@apollo/client";
import client from "../apollo-client";
import Nav from "../components/nav";
import Footer from "../components/footer";
import { MeshBadge, MeshProvider } from "@meshsdk/react";

import "@fontsource/roboto-condensed";
import "@fontsource/inter";

const theme = extendTheme({
  colors: {
    brand: {
      100: "#f7fafc",
      900: "#1a202c",
    },
  },
  fonts: {
    heading: "Roboto Condensed",
    body: "Inter",
  },
  components: {
    Heading: {
      baseStyle: {
        color: "purple.100",
      },
      variants: {
        "page-heading": {
          color: "red.900",
          py: "4",
        },
      },
    },
    Link: {
      baseStyle: {
        color: "orange.200",
      },
    },
  },
});

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <ChakraProvider theme={theme}>
        <MeshProvider>
          <Nav />
          <Box px="5" min-h="100vh" pt="12" pb="48" bg="gray.800" color="white">
            <Component {...pageProps} />
            <Center mt="10">
              <Flex direction="column">
                <Text textAlign="center" py="2">
                  made with
                </Text>
                <Flex direction="row">
                  <Box mx="10">
                    <MeshBadge dark={true} />
                  </Box>
                  <Box mx="10">
                    <a href="https://cardano.org">
                      <Image
                        src="/cardano.png"
                        alt="cardano"
                        width="100"
                        height="100"
                      />
                    </a>
                  </Box>
                  <Box mx="10">
                    <a href="https://dandelion.link">
                      <Image
                        src="/dandelion.jpg"
                        alt="dandelion"
                        width="100"
                        height="100"
                      />
                    </a>
                  </Box>
                </Flex>
              </Flex>
            </Center>
          </Box>
          <Footer />
        </MeshProvider>
      </ChakraProvider>
    </ApolloProvider>
  );
}

export default MyApp;
