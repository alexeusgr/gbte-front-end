import { Box, Heading, Text, List, ListIcon, ListItem } from "@chakra-ui/react";
import { FaQuestionCircle } from "react-icons/fa";
import { getSortedProjectsData } from "../../project-lib/projects";
import TaskStatusList from "../../components/tracking/TaskStatusList";
import { treasury } from "../../cardano/plutus/treasuryContract";
import { NextPage } from "next";

export async function getStaticProps() {
  const allProjectsData = getSortedProjectsData();
  return {
    props: {
      allProjectsData,
    },
  };
}

const DashboardPage: NextPage  = () => {
  return (
    <Box w="80%" mx="auto">
      <Heading py="5" size="2xl">
        Reporting and Data
      </Heading>
      <Text py="5" fontSize="2xl" fontWeight="700">
        The Gimbal Project Treasury and Escrow dapp creates useful data that can
        be used in future decision making.
      </Text>
      <List fontSize="2xl" pb="5">
        <ListItem key="1" pl="5" py="1">
          <ListIcon as={FaQuestionCircle} color="yellow.400" mr="5" />
          How might we use this data to help estimate the cost of future
          projects?
        </ListItem>
        <ListItem key="2" pl="5" py="1">
          <ListIcon as={FaQuestionCircle} color="yellow.400" mr="5" />
          How might the data help developers decide which tasks to take on?
        </ListItem>
        <ListItem key="3" pl="5" py="1">
          <ListIcon as={FaQuestionCircle} color="yellow.400" mr="5" />
          How might the data help treasury issuers to decide on the parameters
          for minting Contribution tokens?
        </ListItem>
        <ListItem key="4" pl="5" py="1">
          <ListIcon as={FaQuestionCircle} color="yellow.400" mr="5" />
          How could this data support reputation-building among devs and across
          projects?
        </ListItem>
      </List>
      <TaskStatusList metadataKey={treasury.metadataKey} />
    </Box>
  );
};

export default DashboardPage;
