import { Box, Heading, Text, Link, Grid, GridItem } from "@chakra-ui/react";
import { getAllProjectIds, getProjectData } from "../../project-lib/projects";
import { Project } from "../../types";
import CommitToProject from "../../components/transactions/commitToProject";
import ContributorTokens from "../../components/contributor/ContributorTokens";

import styles from "../../styles/ProjectPage.module.css";
import { approvalProcess } from "../../project-lib/approvalProcess";

import { GetStaticProps, GetStaticPaths } from "next";
import { ParsedUrlQuery } from "querystring";
import CommitmentNote from "../../components/about-instance/CommitmentNote";

interface IParams extends ParsedUrlQuery {
  id: string;
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { id } = context.params as IParams;

  const projectData = await getProjectData(id);
  return {
    props: {
      projectData,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  let paths: { params: { id: string } }[] = [];

  if (getAllProjectIds) {
    paths = getAllProjectIds();
  }
  return {
    paths,
    fallback: false,
  };
};

type Props = {
  projectData: Project;
};

const ProjectPage: React.FC<Props> = ({ projectData }) => {
  return (
    <Grid templateColumns="repeat(10, 1fr)" gap="5">
      <GridItem colSpan={6} rowSpan={5}>
        <Box bg="gray.700" p="5" fontSize="lg">
          <Heading py="3" size="xl">
            {projectData.title}
          </Heading>
          <Box py="3">
            <div
              dangerouslySetInnerHTML={{ __html: projectData.contentHtml }}
              className={styles.mdStyle}
            />
            <Box border="1px" my="5" />
            <Heading py="3">
              Approval Process:{" "}
              {approvalProcess[projectData.approvalProcess - 1].name}
            </Heading>
            <Text>
              {approvalProcess[projectData.approvalProcess - 1].description}
            </Text>
          </Box>
        </Box>
      </GridItem>
      <GridItem colSpan={4}>
        <CommitmentNote />
      </GridItem>
      <GridItem colSpan={4}>
        {/* A separate Component: */}
        <ContributorTokens />
      </GridItem>
      <GridItem colSpan={4}>
        {/* React allows us to extract this Box to a separate Component */}
        {/* Give it a try on Project 0018 */}
        <Box mb="5" p="5" border="1px" borderRadius="lg">
          <Heading>Project Details</Heading>
          <Text pb="2">
            Project Hash (experimental): {projectData.projectHash}
          </Text>
          <Text pb="2">Project ID: {projectData.id}</Text>
          <Text pb="2">Ada: {projectData.lovelace / 1000000}</Text>
          <Text pb="2">Gimbals: {projectData.gimbals}</Text>
        </Box>
      </GridItem>
      {/* Learn about Chakra UI Grid System: */}
      {/* If you are working on Project 0002, try this: */}
      {/* 1. First, remove the "For Learning" section of <CommitToProject /> */}
      {/* 2. Then, change colSpan in GridItem to move your smaller CommitToProject */}
      {/*    up to the top right side of the page, under "Project Details" GridItem. */}
      {/* Not sure how? Change the colSpan numbers and look at changes to get a feel for how it works. */}
      <GridItem colSpan={10}>
        <CommitToProject projectData={projectData} />
      </GridItem>
    </Grid>
  );
};

export default ProjectPage;
