import { Box, Heading, Text, Grid } from "@chakra-ui/react";
import { getSortedProjectsData } from "../../project-lib/projects";
import type { Project } from "../../types";
import ProjectCard from "../../components/project/ProjectCard";

export async function getStaticProps() {
  const allProjectsData = getSortedProjectsData();
  return {
    props: {
      allProjectsData,
    },
  };
}

type Props = {
  allProjectsData: Project[];
};

const Projects: React.FC<Props> = ({ allProjectsData }) => {
  const frontEndProjects = allProjectsData.filter(
    (project) => project.devCategory === "Front End"
  );
  const plutusProjects = allProjectsData.filter(
    (project) => project.devCategory === "Plutus"
  );
  const dataProjects = allProjectsData.filter(
    (project) => project.devCategory === "Data"
  );
  const documentationProjects = allProjectsData.filter(
    (project) =>
      project.devCategory === "Documentation" ||
      project.devCategory === "Education"
  );
  const contribTokenProjects = allProjectsData.filter(
    (project) => project.devCategory === "Contributor Token"
  );
  const verificationProjects = allProjectsData.filter(
    (project) => project.devCategory === "Verification"
  );

  return (
    <Box w="90%" mx="auto">
      <Heading py="5" size="4xl">
        Gimbal Project Treasury and Escrow
      </Heading>
      <Text p="2" fontSize="xl">
        This initial set of Projects and Tasks provides a hands-on way for
        Contributors to get to know the GPTE system while contributing to its
        development. We will know that our work is successful if other teams are
        able to use, fork, remix or otherwise adapt this Dapp to get stuff done,
        track contributions, and build the reputation of a distributed community
        of Cardano developers.
      </Text>
      <Heading p="2" size="lg">
        Goal #1:
      </Heading>
      <Text p="2" fontSize="xl">
        Make GPTE robust, reliable, and accesible so that other teams can use
        it.
      </Text>
      <Heading p="2" size="lg">
        Goal #2:
      </Heading>
      <Text p="2" fontSize="xl">
        Build the capacity of the Gimbalabs developer community to help other
        teams implement this Dapp.
      </Text>
      <Box my="5" border="1px" />
      <Heading py="5" size="2xl">
        Front End Projects
      </Heading>
      <Grid templateColumns="repeat(3, 1fr)" gap="10">
        {frontEndProjects.map((p: Project) => (
          <ProjectCard key={p.id} project={p} />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Plutus Projects
      </Heading>
      <Grid templateColumns="repeat(3, 1fr)" gap="5">
        {plutusProjects.map((p: Project) => (
          <ProjectCard key={p.id} project={p} />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Verification Projects
      </Heading>
      <Grid templateColumns="repeat(3, 1fr)" gap="5">
        {verificationProjects.map((p: Project) => (
          <ProjectCard key={p.id} project={p} />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Documentation and Education Projects
      </Heading>
      <Grid templateColumns="repeat(3, 1fr)" gap="5">
        {documentationProjects.map((p: Project) => (
          <ProjectCard key={p.id} project={p} />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Contributor Token Projects
      </Heading>
      <Grid templateColumns="repeat(3, 1fr)" gap="5">
        {contribTokenProjects.map((p: Project) => (
          <ProjectCard key={p.id} project={p} />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Data and Tracking Projects
      </Heading>
      <Grid templateColumns="repeat(3, 1fr)" gap="5">
        {dataProjects.map((p: Project) => (
          <ProjectCard key={p.id} project={p} />
        ))}
      </Grid>
    </Box>
  );
};

export default Projects;
