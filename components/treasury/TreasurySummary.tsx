import { useQuery, gql } from "@apollo/client";
import type { UTxO, Asset } from "@meshsdk/core";
import { GraphQLUTxO } from "../../types";

import { Box, Heading, Text, Center, Spinner, Grid } from "@chakra-ui/react";
import { treasury } from "../../cardano/plutus/treasuryContract";
import DistributionCountWidget from "../tracking/DistributionCountWidget";
import ProjectCountWidget from "../tracking/ProjectsCountWidget";

const TREASURY_QUERY = gql`
  query GetTreasuryUTxOs($contractAddress: String!) {
    utxos(where: { address: { _eq: $contractAddress } }) {
      txHash
      index
      address
      value
      tokens {
        asset {
          policyId
          assetName
        }
        quantity
      }
    }
  }
`;

export default function TreasurySummary() {
  const treasuryAddress = treasury.address;
  let gimbalDivisor = 1;
  if(treasury.network == "1") {
    gimbalDivisor = 1000000
  }

  let _contract_utxos: UTxO[] = [];

  const { data, loading, error } = useQuery(TREASURY_QUERY, {
    variables: {
      contractAddress: treasuryAddress,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  if (data) {
    data.utxos.map((utxoFromQuery: GraphQLUTxO) => {
      const assets: Asset[] = [
        {
          unit: "lovelace",
          quantity: utxoFromQuery.value,
        },
        {
          unit:
            utxoFromQuery.tokens[0].asset.policyId +
            utxoFromQuery.tokens[0].asset.assetName,
          quantity: utxoFromQuery.tokens[0].quantity,
        },
      ];

      _contract_utxos.push({
        input: {
          outputIndex: utxoFromQuery.index,
          txHash: utxoFromQuery.txHash,
        },
        output: {
          address: treasuryAddress,
          amount: assets,
        },
      });
    });
  }

  return (
    <Box
      w="90%"
      mx="auto"
      mt="5"
      p="5"
      bg="gray.700"
      borderRadius="lg"
      boxShadow="2xl"
    >
      <Heading color="white">Treasury Instance</Heading>
      {_contract_utxos.length > 0 ? (
        <>
          <Heading size="sm" mt="3" color="white">
            Contract Address: {_contract_utxos[0].output.address}
          </Heading>
          <Grid templateColumns="repeat(2, 1fr)" gap="5" mt="5">
            <Box p="5" bg="gray.900" textAlign="center">
              <Heading size="3xl">
                {parseInt(_contract_utxos[0].output.amount[0].quantity) /
                  1000000}
              </Heading>
              <Text>ada</Text>
            </Box>
            <Box p="5" bg="gray.900" textAlign="center">
              <Heading size="3xl">
                {parseInt(_contract_utxos[0].output.amount[1].quantity) / gimbalDivisor}
              </Heading>
              <Text>gimbals</Text>
            </Box>
            {/* ---- */}
            {/* Here are two quick, hard-coded solutions. To do: implement them! */}
            <ProjectCountWidget />
            <DistributionCountWidget metadataKey="161803" />
            {/* ---- */}
          </Grid>
        </>
      ) : (
        "Not initialized"
      )}
    </Box>
  );
}
