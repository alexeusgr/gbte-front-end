import { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import {
  Flex,
  Spacer,
  Text,
  Center,
  Link as ChakraLink,
} from "@chakra-ui/react";
import { FaTools } from "react-icons/fa";
import { useWallet, useAddress, useNetwork } from "@meshsdk/react";

// To do: replace custom ConnectWallet with Mesh Connect Wallet

export default function Footer() {
  const { connected } = useWallet();
  const address = useAddress();
  const network = useNetwork();
  const [footerColor, setFooterColor] = useState("gray.700");

  useEffect(() => {
    if (connected) {
      setFooterColor("gray.900");
    }
  }, [connected]);

  return (
    <Flex
      pos="fixed"
      bottom="0"
      direction="row"
      w="100%"
      p="5"
      bg={footerColor}
      color="white"
    >
      <ChakraLink href="https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte">
        <Image src="/gitlab.png" alt="GitLab" width="40" height="40" />
      </ChakraLink>
      <Spacer />
      <Center>
        {connected ? (
          <Text>Connected at {address}</Text>
        ) : (
          <Text>Connect a Wallet</Text>
        )}
      </Center>
      <Spacer />
      <Center>
        <Link href="/commitments">
          <FaTools />
        </Link>
      </Center>
    </Flex>
  );
}
