import { Box, Text, Link, Heading } from "@chakra-ui/react";

export default function CommitmentNote() {
  return (
    <Box p="5" mx="auto" bg="yellow.100" color="black">
      <Heading size="md" color="gray.800">
        Please Note:
      </Heading>
      <Text py="2">
        One goal of this initial instance of GPTE is to maximize clarity on
        Project descriptions. If you have questions about a Project of if
        anything is unclear, please create a Post about it in the{" "}
        <Link
          href="https://discord.com/channels/767416282198835220/1023223519649206302"
          target="_blank"
          color="purple.700"
        >
          gbte-discussion Forum on Gimbalabs Discord
        </Link>
        .
      </Text>
      <Text py="2">
        The Project Hash will change every time that we update a Project
        Description, or when reward amounts are changed. This Hash is placed
        on-chain in{" "}
        <Link
          href="https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-front-end/-/blob/main/types/index.ts"
          target="_blank"
          color="purple.700"
        >
          Commitment Tx Metadata
        </Link>
        . Please practice not Commtting to a Project until you have sufficient
        clarity about the task.
      </Text>
      <Text py="2">
        Our opportunity is to use the small-scale, high-trust community we have
        within PPBL to create and test systems that extend to larger-scale
        communities while maintaining trust between Issuers and Contributors.
      </Text>
    </Box>
  );
}
