import { useQuery, gql } from "@apollo/client";

import {
  Box,
  Heading,
  Text,
  Center,
  Spinner,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
} from "@chakra-ui/react";
import {
    getAllEscrowTransactions,
} from "../../project-lib/utils";
import ContributorList from "./ContributorList";
import ProjectStatusList from "./ProjectStatusList";
import EscrowTxList from "./EscrowTxList";

// This component is a reference demo for for building specific data components

// What are the benefits of using one big query like this?
// What are the drawbacks?
const ESCROW_QUERY = gql`
  query TransactionsWithMetadataKey($metadatakey: String!) {
    transactions(where: { metadata: { key: { _eq: $metadatakey } } }) {
      hash
      includedAt
      metadata {
        key
        value
      }
      inputs {
        txHash
        sourceTxIndex
        address
        value
        tokens {
          asset {
            assetId
            assetName
          }
          quantity
        }
      }
      outputs {
        txHash
        index
        address
        value
        tokens {
          asset {
            assetId
            assetName
          }
          quantity
        }
      }
    }
  }
`;

type Props = {
  metadataKey: string;
};

const TaskStatusList: React.FC<Props> = ({ metadataKey }) => {
  // You can import escrow or treasury from /cardano/plutus
  // and use component-level variables any time, depending on your use case, ie:
  // const escrowAddress = escrow.address;
  // const contributorToken = treasury.accessTokenPolicyId
  // const projectToken = treasury.projectTokenAssetId

  const { data, loading, error } = useQuery(ESCROW_QUERY, {
    variables: {
      metadatakey: metadataKey,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  const results = getAllEscrowTransactions(data);

  return (
    <>
      <Box mt="5" p="5" border="1px" borderRadius="lg">
        <Text>
          {results.length} transactions at Metadata Key: {metadataKey}
        </Text>
        <Tabs variant="unstyled" size="lg">
          <TabList>
            <Tab
              borderBottom="1px"
              _selected={{ color: "orange.200" }}
              fontWeight="900"
            >
              View Escrow Transactions
            </Tab>
            <Tab
              borderBottom="1px"
              _selected={{ color: "orange.200" }}
              fontWeight="900"
            >
              View Transactions by Project
            </Tab>
            <Tab
              borderBottom="1px"
              _selected={{ color: "orange.200" }}
              fontWeight="900"
            >
              View Transactions by Contributor Token
            </Tab>
            <Tab
              borderBottom="1px"
              _selected={{ color: "orange.200" }}
              fontWeight="900"
            >
              Raw Escrow Transaction Results
            </Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <EscrowTxList transactions={results} />
            </TabPanel>
            <TabPanel>
              <ProjectStatusList transactions={results} />
            </TabPanel>
            <TabPanel>
              <ContributorList transactions={results} />
            </TabPanel>
            <TabPanel>
              <pre>
                <code className="language-js">
                  {JSON.stringify(results, null, 2)}
                </code>
              </pre>
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </>
  );
};

export default TaskStatusList;
