import Link from "next/link";
import { Project } from "../../types";

import {
  Box,
  Heading,
  Text,
  Center,
  Grid,
  GridItem,
  Link as ChakraLink,
} from "@chakra-ui/react";

type Props = {
  project: Project;
};

const ProjectCard: React.FC<Props> = ({ project: p }) => {
  let projectColor = "gray.700";

  if (p.devCategory === "Data") {
    projectColor = "blue.900";
  }

  if (p.devCategory === "Documentation") {
    projectColor = "teal.900";
  }

  if (p.devCategory === "Education") {
    projectColor = "teal.900";
  }

  if (p.devCategory === "Front End") {
    projectColor = "pink.900";
  }

  if (p.devCategory === "Plutus") {
    projectColor = "purple.900";
  }

  return (
    <GridItem
      key={p.id}
      bg={projectColor}
      border="1px"
      borderColor={projectColor}
      borderRadius="md"
    >
      <Box>
        <Box bg="gray.200" p="2" mt="2">
          <Heading color={projectColor} size="lg">
            {p.title}
          </Heading>
        </Box>
        <Box p="3">
          <Grid templateColumns="repeat(2, 1fr)" gap="5" pb="5">
            <Center flexDir="column" bg="gray.200" color="black">
              <Heading pt="2" color={projectColor}>
                {p.gimbals}
              </Heading>
              <Text py="1"> gimbals</Text>
            </Center>
            <Center flexDir="column" bg="gray.200" color="black">
              <Heading pt="2" color={projectColor}>
                {p.lovelace / 1000000}
              </Heading>
              <Text py="1">ada</Text>
            </Center>
          </Grid>
          <Text py="1" fontSize="xl">
            <ChakraLink href={p.repositoryLink} color="blue.100">
              Link to Repo
            </ChakraLink>
          </Text>
          <Text py="1">
            {p.approvalProcess ? `Approval Process: ${p.approvalProcess}` : ""}
          </Text>
          <Text py="1">Posted: {p.datePosted}</Text>
          <Text py="1">Category: {p.devCategory}</Text>
          <Text py="1">ID: {p.id}</Text>
          {/* next step: javascript branch! */}
          {p.status === "Coming Soon" && (
            <Center py="3" bg="gray.900" color="white">
              Coming Soon
            </Center>
          )}
          {p.status === "Complete" && (
            <Center py="3" bg="gray.900" color="white">
              Complete!
            </Center>
          )}
          {p.status === "Open" && (
            <Link href={`/projects/${p.id}`}>
              <Center
                py="3"
                bg="gray.200"
                color={projectColor}
                _hover={{ bg: "green.100", cursor: "pointer" }}
              >
                View Details {p.id}
              </Center>
            </Link>
          )}
        </Box>
      </Box>
    </GridItem>
  );
};

export default ProjectCard;
