import { useQuery, gql } from "@apollo/client";

import { Box, Heading, Center, Spinner, Text, Link, Grid, GridItem } from "@chakra-ui/react";
import { escrow } from "../../cardano/plutus/escrowContract";
import EscrowTransactionDetails from "../transactions/distributeFromEscrow";

// This component is a reference demo for for building specific data components

// What are the benefits of using one big query like this?
// What are the drawbacks?
const ESCROW_QUERY = gql`
  query UtxosAtEscrowAddress($address: String!) {
    utxos(where: { address: { _eq: $address } }) {
      txHash
    }
  }
`;

const SelectAndDistributeCommitmentUTxO = () => {
  const { data, loading, error } = useQuery(ESCROW_QUERY, {
    variables: {
      address: escrow.address,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <Box mt="5" p="5" border="1px" borderRadius="lg">
      <Grid templateColumns='repeat(2, 1fr)' gap={6}>
        <GridItem>
          <Heading>Commitment UTxOs</Heading>
        </GridItem>
        <GridItem>
          <Box bg="purple.200" color="purple.900" p='2'>To do: Add list of Issuer Tokens here + handle treasury datum.</Box>
        </GridItem>
      </Grid>
      <Text fontSize="xl" py="3">This is a starting point for <Link href="/projects/0008">Project 0008</Link></Text>
      {data.utxos.map((utxo: any) => (
        <Box key={utxo.txHash}>
          <EscrowTransactionDetails txHash={utxo.txHash} />
        </Box>
      ))}
    </Box>
  );
};

export default SelectAndDistributeCommitmentUTxO;
