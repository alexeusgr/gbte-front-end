export declare type GraphQLUTxO = {
    txHash: string,
    index: number,
    address: string,
    value: string,
    tokens: GraphQLToken[],
    datum?: GraphQLInlineDatum,
    script?: GraphQLReferenceScript
}

export declare type GraphQLInputUTxO = {
    txHash?: string,
    sourceTxIndex?: number,
    address: string,
    value: string,
    tokens: GraphQLToken[]
}

export declare type GraphQLToken = {
    asset: {
        policyId: string,
        assetName: string
    }
    quantity: string
}

type GraphQLInlineDatum = {
    bytes: string,
    value: {
        fields: any[],
        constructor: number
    }
}

type GraphQLReferenceScript = {
    type: string,
    hash: string
}

type GraphQLTransactionMetadata = {
    key: string,
    value: any
}

export declare type GraphQLTransaction = {
    inputs: GraphQLInputUTxO[],
    outputs: GraphQLUTxO[],
    metadata: GraphQLTransactionMetadata[]
}
